A browser based book with a pinch of automation. A photographic book from Thomas Whittemore exploration fond. A travel book.

## init

Php server ( for simple includes ) with live reload
Dev environnement should just require Php server ( unless an basic html compilation)

https://gist.github.com/caseywatts/8266527a24667aad80b7d6127130de06
Will use *foreman* to concomitally run php server, browser sync & node sass.

`npm install -g browser-sync`
`npm install -g foreman`
`npm install -g node-sass`

## Start server

`nf start -j Procfile`


## Structure

Index → Master
artefacts 		→ Sacred raw content from external. Do not edit! Period.
book	  		→ A browser-based book in the making
	/ materials → Edited or functional content
	/ assets 	→ Style, library and fonts
export			→ Global PDF and splited one, ready to print

## How to

Whittemore image are stored in the artefact folder. From this fond, a country based selection is call within a json file `book/materials/index_photo.json`.
The `index.php`  parse the json file and embded image path or their relative information in html elements.

Introduction, quote and afterword can be modified according to corpus context or author's willingness.

A map is generated thanks to a Qgis (a cartographic editor) project. 
Cover image is a map overlay by a DEM (Digital Elevation Model). Result is treated as a monochrome bitmap image with Gimp or Photoshop ( or Command line ? ).
Inner map is a simple border map with light relief information. Qgis can generate this relief ( Create a fragment of the DEM layer then Raster → Contour / Outline / Step 50)
To preserve style information, just copy paste them from existing elements.

Once everything is ready, print the book in Chrome as `export/Whittemore.pdf` then split the pdf in booklet thanks to the split.sh bash script. 

` cd export; bash split.sh`

The script is set to produce 16 pages booklet + a 14 pages booklet. This parameters can be easily changed.

## Achtung

I've not upload Artefacts folder for copyright issue...

