 #!/bin/bash

 # linus terminal
 # rename input PDF in.pdf
 # in.pdf must be multiple of 24
 # bash split.sh

FILE='Whittemore.pdf'
COUNTER=1
NUMBEROFPAGES=`pdftk $FILE dump_data |grep NumberOfPages | awk '{print $2}'`
	while [  $COUNTER -lt $NUMBEROFPAGES ]; do
		let PROJECTION=COUNTER+16
		if [ $PROJECTION -lt  $NUMBEROFPAGES ]; then
		    pdftk $FILE cat $COUNTER-$(($COUNTER+15)) output livret/Whittemore-$COUNTER.pdf
		    let COUNTER=COUNTER+16
		else [ $PROJECTION -gt  $NUMBEROFPAGES ]
		    pdftk $FILE cat $COUNTER-$(($COUNTER+13)) output livret/Whittemore-$COUNTER.pdf
		    let COUNTER=COUNTER+14
		fi
	done



