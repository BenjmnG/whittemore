<?php
	require('assets/lib/parsedown/Parsedown.php');
	require('assets/lib/parsedown/ParsedownExtra.php');

	$file = file_get_contents("materials/titles.json");
	$titles = json_decode( $file, true);

	$file = file_get_contents("materials/colophon.json");
	$colophon = json_decode( $file, true);
?>
<!DOCTYPE html>
<html>
	<head>
	    <meta charset="utf-8">
	    <meta name="language" content="en-GB">    
	    <title></title>
	    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
	    <meta http-equiv="content-type" content="text/html; charset=utf-8">
	    <meta name="description" content=""/>
	    <meta name="keywords" content="">

	    <link rel="shortcut icon" href="data:image/x-icon;," type="image/x-icon"> 
	    
	    <link rel="stylesheet" type="text/css" href="assets/font/Bajaderka/stylesheet.css">
	    <link rel="stylesheet" type="text/css" href="assets/font/Imbue/variable/stylesheet.css">
	    <link rel="stylesheet" type="text/css" href="assets/font/Spectral/stylesheet.css">

	    <link rel="stylesheet" type="text/css" href="assets/css-compiled/cover.css">
	</head>

	<body>
		
		<main id="Cover">
			<section id="Front">
			
			<?php
				$split = explode(" ", $titles['title']);
				echo '<p class="collection">'.$titles['collection'].'</p>';

				echo '<h1>'
						.'<span class="minor">'
						.$split[0]." "
						.$split[1]." "
						.$split[2]." "
						.'</span>'
						.$split[3]
						.'</h1>';

				echo '<p class="part">Part&#8239;'.$titles['part'].'</p>';

				echo '<p class="subtitle">'.$titles['subtitle'].'</p>';

			?>

			</section>
			<section id="Spine">
			<?php
				$split = explode(" ", $titles['title']);
				echo '<p class="collection">'.$titles['collection'].'</p>';

				echo '<h1>'
						.'<span class="minor">'
						.$split[0]." "
						.$split[1]." "
						.$split[2]." "
						.'</span>'
						.$split[3]
						.'</h1>';

				//echo '<p class="part">Part&#8239;'.$titles['part'].'</p>';

				echo '<p class="subtitle">'.$titles['subtitle'].'</p>';						
			?>

			</section>

			<section id="Back">
				<figure><img src="../artefacts/photo/Egypt/CDF_BYZWHI_28_111.jpg"></figure>
				<p>BenjmnG.eu</p>
			</section>
			<?php/*
			$svg = file_get_contents('materials/map/generated_map.svg');
			echo $svg;*/
			?>
			<section id="Volet">
				<?php 
				$Parsedown = new Parsedown();
				$text = file_get_contents('materials/volet.md');
				echo $Parsedown->text($text);
				?>
			</section>

			<div id="cropSpine"></div>
			<div id="cropVolet"></div>
		</main>
		
		<script src="assets/js/common.js"></script>
	</body>
</html>     