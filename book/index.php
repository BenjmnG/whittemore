<?php
	require('assets/lib/parsedown/Parsedown.php');
	require('assets/lib/parsedown/ParsedownExtra.php');

	$file = file_get_contents("materials/titles.json");
	$titles = json_decode( $file, true);

	$file = file_get_contents("materials/colophon.json");
	$colophon = json_decode( $file, true);
?>

<!DOCTYPE html>
<html>
	<head>
	    <meta charset="utf-8">
	    <meta name="language" content="en-GB">    
	    <title><?php echo $titles['title'].'&#8239;—&#8239;'.$titles['subtitle']?></title>
	    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
	    <meta http-equiv="content-type" content="text/html; charset=utf-8">
	    <meta name="description" content=""/>
	    <meta name="keywords" content="">

	    <link rel="shortcut icon" href="data:image/x-icon;," type="image/x-icon"> 
	    
	    <link rel="stylesheet" type="text/css" href="assets/font/Bajaderka/stylesheet.css">
	    <link rel="stylesheet" type="text/css" href="assets/font/Imbue/variable/stylesheet.css">
	    <link rel="stylesheet" type="text/css" href="assets/font/Spectral/stylesheet.css">

	    <link rel="stylesheet" type="text/css" href="assets/css-compiled/global.css">
	    <link rel="stylesheet" type="text/css" href="assets/lib/pagedJS/interface-0.1.css">
	</head>

	<body>
		
		<main id="livret">

			<?php
				$split = explode(" ", $titles['title']);
				echo '<p id="fauxTitre">'
						.'<span class="minor">'
						.$split[0]." "
						.$split[1]." "
						.$split[2]." "
						.'</span>'
						.$split[3]
						.'</p>';
			?>
			
			<?php
				echo '<p class="collection">'.$titles['collection'].'</p>';

				echo '<h1>'
						.'<span class="minor">'
						.$split[0]." "
						.$split[1]." "
						.$split[2]." "
						.'</span>'
						.$split[3]
						.'</h1>';

				echo '<p class="part">Part&#8239;'.$titles['part'].'</p>';

				echo '<p class="subtitle">'.$titles['subtitle'].'</p>';

			?>

			<section id="intro">
				<?php 
				$Parsedown = new Parsedown();
				$text = file_get_contents('materials/introduction.md');
				echo $Parsedown->text($text);
				?>				
			</section>


			<nav id="tableOfContent"></nav>			
			<?php echo '<blockquote id="citation">
							<p>'.$titles['cit'].'</p>
							<footer>—&#8239;'.$titles['cit_aut'].', 
							<cite>'.$titles['cit_src'].'</cite></footer>
						</blockquote>'; ?>


			<section id="photography">
				<h2 class="visually-hidden">Photography</h2>
				<?php
						$file = file_get_contents("materials/index_photo.json");
						$index_photo = json_decode( $file, true); 
					
						foreach( $index_photo as $region => $photos) {
							//echo $region;	
							foreach( $photos as $photo => $values) {
								// Hack to get blank page before
								if( $photo != 'CDF_BYZWHI_06_310' || 
									$photo != 'CDF_BYZWHI_06_370' 
								){
							  		echo   '<figure id="'.$photo.'">
							  					<img src="../artefacts/photo/'.$region.'/'.$photo.'.jpg">
							  				</figure>';									
								} else {
									echo '<figure class="blank"></figure>';
								}

								/*if (isset($values['long'])) {
						  			echo '<div class="text"><p class="coord">'.$values['long'].', '.$values['lat'].'</p></div>';
						  		}*/
							  	
							  	// Hack to get blank page after
							  	if( $photo == 'CDF_BYZWHI_06_379' || 
							  		$photo == 'CDF_BYZWHI_06_287' || 
							  		$photo == 'CDF_BYZWHI_06_310' || 
							  		$photo == 'CDF_BYZWHI_06_324' || 
							  		$photo == 'CDF_BYZWHI_06_409' || 
							  		$photo == 'CDF_BYZWHI_06_370' || 
							  		$photo == 'CDF_BYZWHI_06_263' || 
							  		$photo == 'CDF_BYZWHI_06_314'){
									echo '<figure id="'.$photo.'">
						  					<img src="../artefacts/photo/'.$region.'/'.$photo.'.jpg">
						  				</figure>';
								}
							}
						}
					?>
			</section>
				
			<section id="afterword">
				<h2>afterword</h2>
				<?php 
				$Parsedown = new ParsedownExtra();
				$Parsedown->setSafeMode(true);
				$text = file_get_contents('../artefacts/text/Historique-whittemore.md');
				$content = preg_replace('/<h([1-6])>(.*?)<\/h([1-6])>/', '<h3>$2</h3>', $Parsedown->text($text));
				echo $content
				?>
			</section>

			<section class="visually-hidden"></section>
			
			<section id="index">
				<h2>index</h2>
				<p class="disclaimer">Photographies location and reference ordered by page.</p>
				<ul style="position: absolute; bottom: 0;">
				<?php 
					foreach( $index_photo as $region => $photos) {
						foreach( $photos as $photo => $values) {
					  		echo   '<li>';
							echo '<a class="ref" href="#'.$photo.'">Box '.substr($photo, 11, 2).' Item '.substr($photo, 14).'</a>';

							if (isset($values['long'])) {
					  			echo '<p>'.$values['long'].', '.$values['lat'].'</p>';
					  		} else if( strlen($values['location']) > 2){
								echo '<p>unk coordinates</p>';
					  		}

							if (isset($values['location'])) {
					  			echo '<p>'.$values['location'].'</p>';
					  		}
					  		echo '</li>';
						}
					}
				?>
				</ul>
			</section>
			
			<section id="colophon">
				<h2>Colophon</h2>
				<ul>
				<?php 
					echo '<li>'.$colophon['disclaimer'].'</span></li>';
					/*echo '<li>Paper&#8239;:<span>'.$colophon['papier'].'</span></li>';*/
					echo '<li>Typography&#8239;:<ul><li>'.$colophon['type1'].'</li><li>'.$colophon['type2'].'</li><li>'.$colophon['type3'].'</li></ul></li>';
					echo '<li>Documentation&#8239;:<span>'.$colophon['src'].'</span></li><br>';
					echo '<li>'.$colophon['thanks'].'</li>';
					echo '<li>'.$colophon['designer'].'</li>';
					echo '<li>'.$colophon['year'].'</li>';
				?>
				</ul>
			</section>

		</main>
		
		<script src="assets/js/createToc.js"></script>
		<script src="assets/js/regex-typo.js"></script>
		<script src="assets/js/reload-in-place.js"></script>
		<script src="assets/lib/pagedJS/paged.polyfill.js"></script>
		<script src="assets/js/common.js"></script>
	</body>
</html>     