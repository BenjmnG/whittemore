## A long journey

Around 1910 - 1920, Thomas Whittemore undertake several travels to Egypt.<br>From his archaeological work and the legacy we retain nothing here but a few minor photographs. Lost stones whose first arrangement escapes us, scattered on a&nbsp;ground that will be transformed a thousand times over. <br><br>Thus start our walk with Whittemore.