<?php
	require('assets/lib/parsedown/Parsedown.php');
	require('assets/lib/parsedown/ParsedownExtra.php');

	$file = file_get_contents("materials/titles.json");
	$titles = json_decode( $file, true);

	$file = file_get_contents("materials/colophon.json");
	$colophon = json_decode( $file, true);
?>
<!DOCTYPE html>
<html>
	<head>
	    <meta charset="utf-8">
	    <meta name="language" content="en-GB">
	    <title></title>
	    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
	    <meta http-equiv="content-type" content="text/html; charset=utf-8">
	    <meta name="description" content=""/>
	    <meta name="keywords" content="">

	    <link rel="shortcut icon" href="data:image/x-icon;," type="image/x-icon"> 
	    
	    <link rel="stylesheet" type="text/css" href="assets/font/Bajaderka/stylesheet.css">
	    <link rel="stylesheet" type="text/css" href="assets/font/Imbue/variable/stylesheet.css">
	    <link rel="stylesheet" type="text/css" href="assets/font/Spectral/stylesheet.css">

	    <link rel="stylesheet" type="text/css" href="assets/css-compiled/cover.css">
	</head>

	<body>

		<main id="inner_cover">
			<section id="Spine"></section>
			<section id="Volet">
			</section>

			<div id="cropSpine"></div>
			<div id="cropVolet"></div>
			<section id="inner_map">
				<figure>
					<img src="materials/generated_map/egypt_dem-innercover-bitmap.png">
					<img src="materials/generated_map/egypt_map-innercover-type.svg">
				</figure>
				<h2 class="visually-hidden">Map</h2>
			</section>
		</main>
		
		<script src="assets/js/common.js"></script>
	</body>
</html>     